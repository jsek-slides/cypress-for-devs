# Cypress explained for busy developers

## Setup

```shell
npm i
```

### Run on localhost

```shell
npm run watch
```

and in another terminal tab

```shell
npm run serve
```