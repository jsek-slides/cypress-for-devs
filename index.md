---
marp: true
title: Cypress for Developers
description: Cypress - Introduction for Developers
theme: uncover
paginate: true
_paginate: false
---

<style>
@import url(https://fonts.googleapis.com/css?family=Montserrat|PT+Mono);
section {
  font-family: 'Montserrat', 'Segoe UI', Ubuntu, sans-serif;
  letter-spacing: 1px;
  background: #383966;
  color: #fff;
}
img[alt='logo'] {
  background: #fff;
  padding: 2em 25%;
}
a {
  color: #00bcd4;
}
section code {
  font-family: 'PT Mono', Consolas, 'Ubuntu Mono', monospace;
}
li > code,
p > code {
  font-size: .9em;
  background-color: transparent;
  color: #ff9800;
}
pre {
    font-family: 'PT Mono', Consolas, 'Ubuntu Mono', monospace;
    padding: 1.4em 2em;
    filter: invert(100%);
    box-shadow: none;
    font-size: 65%;
    line-height: 1.35;
}
strike {
  color: red;
}
em > strong {
  font-weight: normal;
  font-style: normal;
  color: #ffeb3b;
}

section:after {
    color: #fff!important;
}

.hljs-comment {
    color: #888 !important;
    font-style: italic;
    font-size: .8em;
}
</style>

![logo](images/google/cypress.png)

Introduction for developers

---

![bg right h:85%](images/google/dev-cycle.jpg)

## Who are devs?

<!-- Story: Dominik's PR fixing some regression around Charts -->

---

- **Who** introduces regression?
- **Who** fixes regression?
- **Who** should get alert first?

<br>

Do **we** have the process under control?

---

![bg right h:85%](images/google/humor.jpg)

## What are E2E tests?

<!-- Those that cover our ass -->
<!-- Who wrote at least 10? -->

---

### Tune your expectations

- **High ROI** <br>test code requires maintenance
- **Easy to work with** <br>immediate feedback
- **Easy to fix**

---

![bg left:45%](images/photos/sleepy.jpg)

## Cypress TL;DR

- Easy to start
- Fast & intuitive
- Easy debugging
- Docs quality

<br>

<!-- > No need to learn upfront TypeScript or Cypress -->

---

### Quick start

<!-- (covered in Readme.md) -->

```sh

> npm i
> npm run open
> npx hygen cypress new {name}

```

---

![bg h:90%](images/screenshots/features.png)

---

## Typical flow

1. Outline
1. Focus on one `it()`
   - `xdescribe()`, `xit()`
1. Write scenario
   - Choose interactions `.visit()`, `.click()`, `.type()`
   - Choose selectors `cy.get(...)`
1. Assert

---

## Typical workarounds

```js

/* in case of random timeouts */

cy.wait(500)


/* click failed on visible element */

cy.get('.btn')
  .click({ force: true })

```

---
![bg](#060604)


```js
describe('[Newsletter form]', () => {

  describe('open/close', () => {
    it('should open newsletter popup', () => {})
    it('should close newsletter popup', () => {})
  })

  describe('validation', () => {
    it('should not allow empty email', () => {})
    it('should not allow invalid email', () => {})
    it('should accept and send valid email', () => {})
    it('should be disabled while loading', () => {})
  })

  describe('error handling', () => {
    it('should alert if submission was rejected', () => {})
  })
})
```

---
![bg](#060604)


```js
describe('[Newsletter form]', () => {

  beforeEach(() => {
    cy.visit('/blog.html');
    cy.get('.newsletter-show').click()
  })

  describe('open/close', () => {
    it('should open newsletter popup', () => {
      cy.get('.newsletter-signup').should('be.visible')
    })

    it('should close newsletter popup', () => {
      cy.get('.modal-close'      ).click()
      cy.get('.newsletter-signup').should('not.be.visible')
    })
  })
})
```

---

#### Tip #1

Less `it(...)`'s

<!-- because performance matters -->

---
![bg](#060604)


```js
describe('[Newsletter form]', () => {

  beforeEach(() => {
    cy.visit('/blog.html');
  })

  describe('open/close', () => {
    it('should open and close newsletter popup', () => {
      cy.get('.newsletter-show'  ).click()
      cy.get('.newsletter-signup').should('be.visible')
      cy.get('.modal-close'      ).click()
      cy.get('.newsletter-signup').should('not.be.visible')
    })
  })
})
```

---

#### Tip #2

Extract helpers to increase readability

---

![bg](#060604)

```js
attributes.ExpandGroup('Geography');
attributes.Select('Country');
attributes.SelectGroup('Geography');

grid.ShowResults();
chart.Toggle();

chartDisplaysColumns(21);
legendItemExists('Algeria', 0);
legendItemExists('Rest of Data', 'last');
```

---

#### Tip #3

Stubbing server

<br>

- **Wait** for some XHR call to finish
- **Delay** response to validate _loading_ states
- **Choose** fixtures from `*.json` files
- **Fail** response to validate _error handling_

<br>

---

![bg](images/photos/karmel.jpg)

---

```js

cy.server()
  .route('GET', '**/Attributes?*')
  .route('POST', '**/Query')

```

---
![bg](#060604)


```js
cy.server()
  .route('GET', '**/Attributes?*').as('loadAttributes')

cy.visit('/editQuery')

cy.wait('loadAttributes')
```

---
![bg](#060604)


```js
// scales better with route helper

routes.expect('loadAttributes')

cy.visit('/editQuery')

cy.wait('loadAttributes')
```

---
![bg](#060604)


```js
cy.server()
  .route('GET', '**/Attributes?*',
    'fixture:attributes.json'
  )
  .route('POST', '**/Query',
    'fixture:query.json'
  )
```

---

![bg](#060604)


```js
cy.route({
    method: 'POST',
    url: '**/CheckName',
    response: {
      CheckedName: 'Untitled query',
      AllowedName: 'VALID NAME'
    }
  })

cy.get('[qa=save-query] button')
  .click()

cy.get('[qa=saved-query-name]')
  .should('have.value', 'VALID NAME')
```

---

![bg](#060604)


```js
cy.route({
    method: 'POST',
    url: 'https://emailoctopus.com/**',
    response: { error: "Call failure" },
    status: 400
  })

cy.get('input[type=email]')
  .type('valid_email@example.com')

cy.get('button[type=submit]')
  .click()

cy.get('.signup-error-message')
  .should('be.visible')
```

---

![bg right:33%](images/google/dirty-work.jpg)

### Understand the "why"

Why developers?
Why now?

---

![bg left:33%](images/google/farm.jpg)

## There is no excuse

- It is **easier** then ever
- It benefits **the team**
- It **scales** better

<!-- even easier than unit tests -->

---

Watch : [The Principles of Clean Architecture](https://youtu.be/o_TH-Y78tt4?t=3501) (2:30)

<br>

🡇

<br>

- Test suite passes &nbsp; 🡒 &nbsp; Ready to deploy
- Fast tests + coverage &nbsp; 🡒 &nbsp; Healthy codebase
<br>
- Test run always red &nbsp; 🡒 &nbsp; No tests
- No tests &nbsp; 🡒 &nbsp; Fear

---

![bg](images/photos/fear.jpg)

---

### Obstacles

- Expecting testers to write **all** e2e tests
  - results in 100000 LOC of total mess <!-- We need to work together! -->
<br>

- Expecting testers to maintain **all** e2e tests
  - results in always-red test runs → useless

---

![bg right:33%](images/google/perspective.jpg)

### True value of QA

- add perspective
- verify acceptance criteria
- manual regression (faster)
- initial troubleshooting

---

![bg left:33%](images/google/volunteer.jpg)

### Homework / OKR

<strike>How can I get my task done?</strike>

_**How can I **make my work better**?**_

<!-- realize  that TEAM matters -->
<!-- understand your role -->
<!-- may the force be with you -->

---

## Question everything

when a concept is old...<br>just let it go

---

![](images/google/pyramid.png)

---

# ROI

write _**__just enough__**_ tests<br>that **bring value**

---

\*

Try it for **TDD**

<br>

---

<br>

### Resources

<br>

[Intro](https://www.cypress.io/how-it-works/) | [Docs](https://docs.cypress.io/guides/ore-concepts/test-runner.html) | [FAQ](https://docs.cypress.io/faq/questions/using-cypress-faq.html)

<br>